class Fibonacci
  def initialize
    @memory = []
  end
  
  def fibonacci(n)
    raise ArgumentError unless n.instance_of?(Integer) && n >= 0

    return 0 if n == 0
  
    return 1 if n == 1
  
    return 1 if n == 2

    if @memory[n].nil?
      @memory[n - 1] = fibonacci(n - 1) if @memory[n - 1].nil?
      @memory[n - 2] = fibonacci(n - 2) if @memory[n - 2].nil?
      @memory[n] = @memory[n - 1] + @memory[n - 2]
    end
  
    return @memory[n]
  end
end

