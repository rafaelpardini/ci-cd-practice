require 'fibonacci'

RSpec.describe Fibonacci, '#fibonacci' do
  fib = Fibonacci.new
  it 'should give the correct result' do
    expect(fib.fibonacci(0)).to eq 0
    expect(fib.fibonacci(1)).to eq 1
    expect(fib.fibonacci(2)).to eq 1
    expect(fib.fibonacci(3)).to eq 2
    expect(fib.fibonacci(4)).to eq 3
    expect(fib.fibonacci(10)).to eq 55
  end
  it 'should raise ArgumentError when passing a string' do
    expect { fib.fibonacci('string') }.to raise_error ArgumentError
  end
end

